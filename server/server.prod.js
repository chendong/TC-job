/**
 * Created by Melon on 2016/10/21.
 */
var path = require('path')
var express = require('express')

var app = express()

app.use(express.static(path.join(__dirname, '../dist'))) //提供dist下的静态文件

app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname, '../dist/index.html'))
})

module.exports=app



