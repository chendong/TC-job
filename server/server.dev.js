/**
 * Created by Melon on 2016/10/21.
 */
var path = require('path')
var express = require('express')
var webpack = require('webpack')
var webpackDevMiddleware = require('webpack-dev-middleware'); //只针对于开发环境
var webpackHotMiddleware = require('webpack-hot-middleware'); //模块热替换（HMR）

var webpackConfig = require('../webpack.config.dev')

var app = express()
var compiler = webpack(webpackConfig)

app.use(webpackDevMiddleware(compiler, {
    // publicPath is required, whereas all other options are optional

    noInfo: true,
    // display no info to console (only warnings and errors)

    quiet: false,
    // display nothing to the console

    lazy: true,
    // switch into lazy mode
    // that means no watching, but recompilation on every request

    watchOptions: {
        aggregateTimeout: 300,
        poll: true
    },
    // watch options (only lazy: false)

    publicPath: webpackConfig.output.publicPath,
    // publices path to bind the middleware to
    // use the same as in webpack

    index: "index.html",
    // the index path for web server

    // headers: { "X-Custom-Header": "yes" },
    // custom headers

    stats: {
        colors: true
    },
    // options for formating the statistics

    reporter: null,
    // Provide a custom reporter to change the way how logs are shown.

    serverSideRender: false,
    // Turn off the server-side rendering mode. See Server-Side Rendering part for more info.
}))


app.use(webpackHotMiddleware(compiler))
app.get('*', function(req, res) {
  res.sendFile(path.join(__dirname, '../dist/index.html'))

})

module.exports=app
