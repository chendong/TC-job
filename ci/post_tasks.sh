#!/bin/bash

PROJECT_NAME=mg-web
DIR_DAEMON=/data/mgang/${PROJECT_NAME}-ci
DIR_PACKAGE=/data/package/mgang/
BUILDTIME=$(date "+%Y%m%d.%H%M%S")
VERSION=1.0.0

cd /data/src/${PROJECT_NAME}-ci/
rm -fr dist
git pull
npm run build:ci


rm -fr ${DIR_DAEMON}/*
cp -r dist/* ${DIR_DAEMON}/
cp -r favicon.ico ${DIR_DAEMON}/

