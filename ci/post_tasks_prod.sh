#!/bin/bash

PROJECT_NAME=mg-web
DIR_PACKAGE=/data/package/mgang/
BUILDTIME=$(date "+%Y%m%d.%H%M%S")
VERSION=1.0.0

cd /data/src/${PROJECT_NAME}/
rm -fr dist
git pull
npm run build:production



cp -r favicon.ico dist/
tar cvfz ${DIR_PACKAGE}/${PROJECT_NAME}-production-${VERSION}.${BUILDTIME}.tar.gz -C dist .
