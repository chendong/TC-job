/**
 * Created by Melon on 2016/10/22.
 */
import React, {Component, PropTypes} from 'react'
import {Link, browserHistory} from 'react-router'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import Helmet from "react-helmet"

import * as UserActions from '../redux/actions/UserActions'
import * as CommonActions from '../redux/actions/CommonActions'
import Empty from '../utils/emptyUtil'

import '../assert/style/login.scss'
import Img1 from '../assert/image/banner/banner-login.jpg'
import ImgLogo from '../assert/image/logo.png'

var src = 'http://steelgang-pro.oss-cn-beijing.aliyuncs.com/avatar/avatar_default_1.jpg'

class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      registering: false,
      next: false,
      image: ''
    }
    this.login = this.login.bind(this)
    this.registerModalShow = this.registerModalShow.bind(this)
    this.register = this.register.bind(this)
    this.choosePhoto = this.choosePhoto.bind(this)
    this.writeProfile = this.writeProfile.bind(this)
    this.Next = this.Next.bind(this)
  }

  componentWillMount() {

  }

  componentDidMount() {

  }
  componentWillUnmount(){
    this.props.actions.acPicUploadReset()
  }

  componentWillReceiveProps(nextProps) {
    //注册成功,隐藏信息填写，登录并跳转到首页
    if (this.props.registerStatus.type === 'loading' && nextProps.registerStatus.type === 'success') {
      $('.modal.profile').modal('hide');
       // this.login($('.reaccount').val(),$('.passWord').val())
        browserHistory.replace('/')//回到首页
    }
    //注册失败,隐藏信息填写
    if (this.props.registerStatus.type === 'loading' && nextProps.registerStatus.type === 'error') {
      $('.modal.profile').modal('hidden');
    }
    //登录成功,隐藏信息填写,
    if (this.props.loginStatus.type === 'loading' && nextProps.loginStatus.type === 'success') {
      $('.modal.profile').modal('hide');
      browserHistory.replace('/') //回到首页
    }
    //上传头像成功
    if (Empty.check(nextProps.picUploadUrl)) {
      //console.log(pictureFile.pitureUrl)
        console.log($('.nickName').val())
      this.setState({image: nextProps.picUploadUrl.pitureUrl})
    }
  }

  //登录
  login(account,password) {
    let params = {
      account: account,
      password: password,
    }
    this.props.actions.acLogin(params)
      //browserHistory.replace('/') //回到首页
  }

  //弹出注册Modal
  registerModalShow() {
    $('.modal.sign').modal('show');
  }

  Next() {
    if (Empty.check($('.reaccount').val()) && Empty.check($('.passWord').val()) && Empty.check($('.repassWord').val())) {
      this.setState({next: true})
    } else {
      this.setState({next: false})
    }
  }

  //注册
  register() {
    if (this.state.next) {
      this.setState({registering: true})
      setTimeout(() => {
        this.setState({registering: false})
        $('.modal.sign').modal('hiden');
        $('.modal.profile').modal('show');
      }, 1000)
    }
  }

  //填写个人信息
  writeProfile() {
    //console.log($("[name='nickName']").val())
    if(Empty.check($('.nickName').val())) {
        let params = {
            account: $('.reaccount').val(),
            passWord: $('.passWord').val(),
            nickName: $('.nickName').val(),
            head: this.state.image,
            sex: $('[name="sex"]').val()
        }
        this.props.actions.asyncRegister(params)
    }
  }

  choosePhoto(e) {
    e.preventDefault()
    let files
    if (e.dataTransfer) {
      files = e.dataTransfer.files;
    } else if (e.target) {
      files = e.target.files;
    }
    if (Empty.check(files)) {
        /*setTimeout(() => {
            this.setState({image: 'https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=2001435010,1450265306&fm=23&gp=0.jpg'})
        },3000)*/
            this.props.actions.acPicUpload(files[0])
    }
  }

  render() {
    let {loginStatus, registerStatus} = this.props;
    return (
        <div className="login-container">
          <Helmet title="登录"/>
          {
            loginStatus.type === 'error' ? (() => {
              return (
                  <div className="ui tiny yellow message show"
                       onClick={() => this.props.actions.asyncStatus({key: 'login', type: 'reset'})}>
                    <i className="close icon"></i>
                    <div className="header">{loginStatus.msg}</div>
                  </div>)
            })() : false
          }

          {
            registerStatus.type === 'error' ? (() => {
              return (
                  <div className="ui tiny yellow message show"
                       onClick={() => this.props.actions.asyncStatus({key: 'register', type: 'reset'})}>
                    <i className="close icon"></i>
                    <div className="header">{loginStatus.msg}</div>
                  </div>)
            })() : false
          }

          <div className="bg-logo" onClick={()=>browserHistory.replace('/')}/>

          {/*登录表单*/}
          <div className="login-content">
            <div className="ui segment">
              <form className="ui form">
                <div className="field">
                  <label>账号:</label>
                  <input type="text" name="account" placeholder="账号"/>
                </div>
                <div className="field">
                  <label>密码:</label>
                  <input type="password" name="password" placeholder="密码"/>
                </div>
                <div className="field">
                  <div className="ui checkbox">
                    <input type="checkbox" tabIndex="0" defaultChecked/>
                    <label>记住密码</label>
                  </div>
                </div>
                {
                  loginStatus.type === 'loading' ?
                      <button className="ui primary loading button">Loading</button> :
                      <div className="ui green button" onClick={()=>this.login($('[name="account"]').val(),$('[name="password"]').val())}>登录</div>
                }
                <div className="ui submit button" onClick={this.registerModalShow}>注册</div>
              </form>
            </div>
          </div>

          {/*注册modal*/}
          <div className="ui small modal sign">
            <div className="header">注册</div>
            <div className="content">
              <form className="ui form">
                <div className="field">
                  <label>手机号</label>
                  <input onChange={() => this.Next()} className="reaccount" name="reaccount" placeholder="手机号" type="text"/>
                </div>
                <div className="field">
                  <label>密码</label>
                  <input onChange={() => this.Next()} className="passWord" name="passWord" placeholder="密码(至少为4位)"
                         type="password"/>
                </div>
                <div className="field">
                  <label>确认密码</label>
                  <input onChange={() => this.Next()} className="repassWord" name="repassWord" placeholder="确认密码(至少为4位)"
                         type="password"/>
                </div>
              </form>
            </div>
            <div className="actions">
              <div className="ui ok button">取消</div>
              {
                this.state.registering ?
                    <button className="ui primary loading button">Loading</button> :
                    <div className={this.state.next ? "ui green button" : "ui button"} onClick={this.register}>
                      填写个人信息</div>
              }
            </div>
          </div>

          {/*注册成功后填写用户昵称以及上传头像model*/}
          <div className="ui small modal profile">
            <div className="header">填写个人信息</div>
            <div className="content">
              <form className="ui form">
                <div className="field">
                  <label>昵称</label>
                  <input className="nickName" name="nickName" placeholder="输入昵称" type="text"/>
                </div>
                <div className="inline fields">
                  <label>性别:</label>
                  <div className="field">
                    <div className="ui radio checkbox">
                      <input type="radio" name="sex" value="男" defaultChecked/>
                      <label>男</label>
                    </div>
                  </div>
                  <div className="field">
                    <div className="ui radio checkbox">
                      <input type="radio" name="sex" value="女"/>
                      <label>女</label>
                    </div>
                  </div>
                  <div className="field">
                    <div className="ui radio checkbox">
                      <input type="radio" name="sex" value="secret"/>
                      <label>保密</label>
                    </div>
                  </div>
                </div>
                <div className="field">
                  <label>头像</label>
                  <div className="item" style={{height: 100}}>
                    <input ref={(ref) => this.image = ref} onChange={(e) => this.choosePhoto(e)} type="file" style={{
                      position: 'absolute',
                      outline: 'none',
                      width: 98,
                      height: 36,
                      opacity: 0,
                      cursor: 'pointer'
                    }}/>
                    <button className="ui primary button">选择图片</button>
                    <img ref={(ref) => this.img = ref} src={Empty.check(this.state.image) ? this.state.image : src}
                         style={{position: 'absolute', left: 110, height: 100, width: 100}}/>
                  </div>
                </div>
              </form>
            </div>
            <div className="actions">
              {
                registerStatus.type === 'loading' ?
                    <button className="ui primary loading button">Loading</button> :
                    <div className="ui green button" onClick={this.writeProfile}>确认</div>
              }
            </div>
          </div>

        </div>
    )
  }
}


function mapStateToProps(state) {
  return {
    myProfile: state.user.myProfile,
    loginStatus: state.common.asyncStatus.login || {},
    registerStatus: state.common.asyncStatus.register || {},
    picUploadUrl: state.common.picUploadUrl
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({...UserActions, ...CommonActions}, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)