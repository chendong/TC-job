/**
 * Created by Melon on 2016/10/22.
 */
import React, {Component, PropTypes} from 'react'
import {Link, browserHistory} from 'react-router'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import Helmet from "react-helmet"

import * as UserActions from '../redux/actions/UserActions'
import * as ArticleActions from '../redux/actions/ArticleActions'
import * as CommonActions from '../redux/actions/CommonActions'
import Empty from '../utils/emptyUtil'
import HeadComponent from '../components/HeadComponent'
// import '../assert/style/Detail.scss'
import Img1 from "../assert/image/user-head.png"
import Img2 from '../assert/image/head-example/elliot.jpg'
import Img3 from '../assert/image/head-example/helen.jpg'
import Img4 from '../assert/image/head-example/steve.jpg'
var Head = 'http://steelgang-pro.oss-cn-beijing.aliyuncs.com/avatar/avatar_default_1.jpg'
var time = new Date()
class Detail extends Component {
  constructor(props) {
    super(props)
    this.state = {
      commentType:false,
        type:''
    }
    this.comment = this.comment.bind(this)
  }

  componentWillMount() {
      let {myProfile} = this.props
      let articleId = this.props.params.id;
      //判断cookie是否有userId,如果有,表示已登录，使用userId去获取用户个人信息
      if(!Empty.check(myProfile)){
          let cookies = document.cookie.split(';');
          for (let i = 0; i < cookies.length; i++) {
              let cookieKey = cookies[i].split('=')[0];                 // cookies[i] name=value形式的单个Cookie
              if ('userId' === cookieKey) {
                  let userId = cookies[i].split('=')[1];
                  if(Empty.check(userId)) {
                      return this.props.actions.acMyProfile(userId)
                  }
              }
          }}
    this.props.actions.acArticleDetail({articleId: articleId})
  }

  componentDidMount() {

  }

  componentWillReceiveProps(nextProps) {

  }

  componentDidUpdate(prevProps, prevState) {

  }

  comment() {
    let {myProfile} = this.props
      if(Empty.check(myProfile)) {
          if (Empty.check($('[name="comment"]').val())) {
              let params = {
                  userId: myProfile.id,
                  nickName: myProfile.nickName,
                  head: myProfile.head,
                  context: $('[name="comment"]').val(),
              }
              this.setState({commentType: true})
              setTimeout(() => {
                  this.setState({commentType: false})
                  this.props.actions.acCommentArticle(params);
                  $('[name="comment"]').val('')
              }, 2000)
          }else {
            this.setState({type:'输入内容不能为空～'})
          }
      }else {
      this.setState({type:'请先登录～'})
      }
  }

  render() {
      let articleId = this.props.params.id;
    let {createArticles,articleDetail} = this.props;

    return (
        <div className="ui container container-home" style={{paddingBottom:50}}>
          <Helmet title="文章详情"/>
            {!Empty.check(this.state.type)?false
                :
                <div className="ui tiny yellow message show"
                     onClick={() => this.setState({type:''})}>
                  <i className="close icon"></i>
                  <div className="header">{this.state.type}</div>
                </div>}
          <HeadComponent/>
          {/*文章详情*/}
          <div className="ui divided items">
          {
            Empty.check(createArticles[articleId]) ? (() => {
              let  article = createArticles[articleId]
              return (
                    <div className="item">
                      <div className="ui tiny image" style={{textAlign:'center'}}>
                        <img src={article.head}/>
                        <span className="price">{article.nickName}</span>
                      </div>
                      <div className="content">
                        <div className="header">{article.title}</div>
                        <div className="description">
                          <p>{article.context}</p>
                        </div>
                          {Empty.check(article.image)?<img src={article.image} style={{width:500,height:400}}/>:false}
                      </div>
                    </div>
              )
            })() : false
          }
              {Empty.check(articleDetail)?(() => {
                      let  article = articleDetail
                      return (
                          <div className="item">
                              <div className="ui tiny image" style={{textAlign:'center'}}>
                                  <img src={Empty.check(article.head)?article.head:Head}/>
                                  <span className="price">{article.userNickName}</span>
                              </div>
                              <div className="content">
                                  <div className="header">{article.title}</div>
                                  <div className="description">
                                      <p>{article.context}</p>
                                  </div>
                              </div>
                          </div>
                      )
                  })() : false}
          </div>
          {/*评论*/}

          <div className="ui comments" style={{marginLeft:101}}>
            <h3 className="ui dividing header">评论</h3>

            {
              Empty.check(articleDetail.comments)? (()=> {
               return articleDetail.comments.map((comment, index, comments) => {
                  console.log('----------------------')
                  return (
                    <div className="comment" key={index}>
                      <a className="avatar">
                        <img src={Head}/>
                      </a>
                      <div className="content">
                        <a className="author">{comment.nickName}</a>
                        <div className="metadata">
                          <span className="date">{time.getHours()+':'+time.getMinutes()}</span>
                        </div>
                        <div className="text">{comment.context}</div>
                        <div className="actions">
                          <a className="reply">评论</a>
                        </div>
                      </div>
                    </div>
                  )
                })
              })() :  false
            }

            <form className="ui reply form">
              <div className="field">
                <textarea rows="2" name="comment"></textarea>
              </div>
                {this.state.commentType?
                    <div className="ui blue labeled submit loading icon button" onClick={this.comment}><i className="icon edit"></i> 回复 </div>:
                    <div className="ui blue labeled submit icon button" onClick={this.comment}><i className="icon edit"></i> 回复 </div>
                }
            </form>
          </div>

        </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    myProfile: state.user.myProfile,
    articleDetail: state.article.articleDetail,
      createArticles:state.article.createArticles
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({...UserActions, ...ArticleActions, ...CommonActions}, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Detail)