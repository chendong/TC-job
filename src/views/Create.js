/**
 * Created by Melon on 2016/10/22.
 */
import React, {Component, PropTypes} from 'react'
import {Link, browserHistory} from 'react-router'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import Helmet from "react-helmet"

import * as UserActions from '../redux/actions/UserActions'
import * as ArticleActions from '../redux/actions/ArticleActions'
import * as CommonActions from '../redux/actions/CommonActions'
import Empty from '../utils/emptyUtil'
import HeadComponent from '../components/HeadComponent'

class Create extends Component {
  constructor(props) {
    super(props)
    this.state = {
        registering: false,
        image:'',
    }
    this.create=this.create.bind(this)
      this.choosePhoto = this.choosePhoto.bind(this)
  }

  componentWillMount() {
      let {myProfile} = this.props
      //判断cookie是否有userId,如果有,表示已登录，使用userId去获取用户个人信息
      if(!Empty.check(myProfile)){
          let cookies = document.cookie.split(';');
          for (let i = 0; i < cookies.length; i++) {
              let cookieKey = cookies[i].split('=')[0];                 // cookies[i] name=value形式的单个Cookie
              if ('userId' === cookieKey) {
                  let userId = cookies[i].split('=')[1];
                  if(Empty.check(userId)) {
                      return this.props.actions.acMyProfile(userId)
                  }
              }
          }}

  }

    componentWillReceiveProps(nextProps) {
        //注册成功,隐藏信息填写，登录并跳转到首页
        let {picUploadUrl,createArticleType} = nextProps
        if (Empty.check(picUploadUrl)) {
            this.setState({image: picUploadUrl.pitureUrl})
        }
        if(Empty.check(createArticleType)&&Empty.check(createArticleType.type!='loading')){
           this.setState({registering:false})
            if(createArticleType.type=='success'){
               browserHistory.replace('/')
                this.props.actions.acArticleList()
            }
        }
        console.log(createArticleType)
    }
  componentDidMount() {
    let {myProfile} = this.props
   if (!Empty.check(myProfile)) {
      browserHistory.replace('login')
    }
  }

  componentDidUpdate(prevProps, prevState) {

  }
  choosePhoto(e) {
        e.preventDefault()
        let files
        if (e.dataTransfer) {
            files = e.dataTransfer.files;
        } else if (e.target) {
            files = e.target.files;
        }
        if (Empty.check(files)) {
           /* setTimeout(() => {
                this.setState({image:'http://www.ruanyifeng.com/blogimg/asset/2016/bg2016092101.jpg'})
            }, 2000)*/
            this.props.actions.acPicUpload(files[0])
        }
    }

  create() {
    let {myProfile} = this.props
      this.setState({registering: true})
      if(Empty.check(this.state.image)) {
          setTimeout(() => {
              this.setState({registering: false})
              let params = {
                  title: $('[name="title"]').val(),
                  context: $('[name="context"]').val(),
                  image: this.state.image,
                  head: myProfile.head,
                  nickName: myProfile.nickName,
                  userId:myProfile.userId,
                  delete: true
              }
              let articles = this.props.createArticles
              articles.unshift(params)
              this.props.actions.acFCAU(articles)
              browserHistory.replace('/') //回到首页
          }, 3000)
      }else {
          let params = {
              userId: myProfile.id,
              title: $('[name="title"]').val(),
              context: $('[name="context"]').val(),
          }
          this.props.actions.acCreateArticle(params)
      }
  }

  render() {
    return (
        <div className="ui container container-home">
          <Helmet title="发表"/>
          <HeadComponent/>
          <div className="ui form">
            <div className="field">
              <label>标题</label>
              <input name="title" type="text" placeholder="输入标题"/>
            </div>
            <div className="field">
              <label>内容</label>
              <textarea name="context"></textarea>
            </div>
            <div className="field">
              <input ref={(ref) => this.image = ref} onChange={(e) => this.choosePhoto(e)} type="file" style={{
                  position: 'absolute',
                  outline: 'none',
                  width: 98,
                  height: 36,
                  opacity: 0,
                  cursor: 'pointer'
              }}/>
              <button className="ui primary button">选择图片</button>
                {Empty.check(this.state.image)?
                    <img ref={(ref) => this.img = ref} src={this.state.image}
                         style={{position: 'absolute', left: 110, height: 200, width: 200}}/>:
                false}
            </div>
          </div>
          <div className="ui segment" style={{marginTop:200}}>
              {this.state.registering?
                  <button className="fluid ui green loading button" onClick={this.create}>发表中</button>
              :
                  <button className="fluid ui green button" onClick={this.create}>发表</button>}
          </div>
        </div>
    )
  }
}

function mapStateToProps(state) {
  return {
      myProfile: state.user.myProfile,
      picUploadUrl: state.common.picUploadUrl,
      createArticles:state.article.createArticles,
      createArticleType:state.common.asyncStatus.createArticleList || {},
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({...UserActions, ...ArticleActions, ...CommonActions}, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Create)