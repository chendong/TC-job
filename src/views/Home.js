/**
 * Created by Melon on 2016/10/22.
 */
import React, {Component, PropTypes} from 'react'
import {Link, browserHistory} from 'react-router'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import Helmet from "react-helmet"

import * as UserActions from '../redux/actions/UserActions'
import * as ArticleActions from '../redux/actions/ArticleActions'
import * as CommonActions from '../redux/actions/CommonActions'
import Empty from '../utils/emptyUtil'
import HeadComponent from '../components/HeadComponent'
import '../assert/style/Home.scss'
import Img1 from "../assert/image/user-head.png"
import Img2 from '../assert/image/head-example/elliot.jpg'
import Img3 from '../assert/image/head-example/helen.jpg'
import Img4 from '../assert/image/head-example/steve.jpg'
var Head = 'http://steelgang-pro.oss-cn-beijing.aliyuncs.com/avatar/avatar_default_1.jpg'

class Cell extends Component{
  constructor(props){
    super(props)
      this.state = {
      active:false
      }
      this.click = this.click.bind(this)
  }
  click(){
    let {profile,changeType} = this.props
      changeType()
      if(Empty.check(profile)) {
          this.setState({active: !this.state.active})
      }
  }
  render() {
    let {index,onDelete,articleId,profile} = this.props
      //console.log(articleId)
      //console.log(profile.id)
    return(
<div className="meta">
<span className="price" onClick={()=>this.click()}>{this.state.active&&Empty.check(profile)?<i className="like icon active"></i>:<i className="like icon"></i>}</span>
    {Empty.check(articleId==profile.id)&&Empty.check(profile.userId)?<span className="price" style={{marginLeft:50,cursor:'pointer'}} onClick={()=>onDelete(index)}>
  <i className="trash outline icon"></i>
  </span>:false}
</div>
)
}
}

class Home extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data:[],
      type:true
    }
    this.onDelete = this.onDelete.bind(this)
    this.changeType = this.changeType.bind(this)
  }

  componentWillMount() {
    let {articleList,myProfile} = this.props
    if (!Empty.check(articleList)) {
      this.props.actions.acArticleList()
    }
    //判断cookie是否有userId,如果有,表示已登录，使用userId去获取用户个人信息
      if(!Empty.check(myProfile)){
   let cookies = document.cookie.split(';');
    for (let i = 0; i < cookies.length; i++) {
      let cookieKey = cookies[i].split('=')[0];                 // cookies[i] name=value形式的单个Cookie
      if ('userId' === cookieKey) {
        let userId = cookies[i].split('=')[1];
        if(Empty.check(userId)) {
            return this.props.actions.acMyProfile(userId)
        }
      }
    }}
  }


  componentDidMount() {
      let {createArticles} = this.props
      this.setState({data:createArticles})
  }
    componentWillReceiveProps(nextProps){
    let {createArticles} = nextProps
        Empty.check(createArticles)?
        this.setState({data:createArticles}):false
    }
  componentDidUpdate(prevProps, prevState) {

  }
  onDelete(index){
        setTimeout(()=>{ this.state.data.splice(index,1)
      this.setState({data:this.state.data})
      this.props.actions.acFCAU(this.state.data)},2000)
  }
  changeType(){
    let {myProfile} = this.props
     if(!Empty.check(myProfile)){
      this.setState({type:false})
     }
  }
  render() {
    let {articleList,createArticles,myProfile} = this.props;
    return (
        <div className="ui container container-home" style={{paddingBottom:50}}>
          <Helmet title="优友"/>
            {this.state.type?false
                :
                <div className="ui tiny yellow message show"
                                        onClick={() => this.setState({type:true})}>
                  <i className="close icon"></i>
                  <div className="header">请先登录~</div>
                </div>}
          <HeadComponent/>
            {/*文章列表*/}
            <div className="ui divided items">
                {
                    articleList.map((article, index, articleList) => {
                        return (
                            <div className="item" key={index+100}>
                                <div className="ui tiny image" style={{textAlign:'center'}}>
                                    <img src={Empty.check(article.head)?article.head:Head}/>
                                    <span className="price">{article.userNickName}</span>
                                </div>
                                <div className="content">
                                    <div className="header">{article.title}</div>
                                    <Cell changeType={this.changeType} profile={myProfile} index={index} onDelete={this.onDelete} articleId={article.userId}/>
                                    <div className="description" style={{cursor:'pointer'}} onClick={()=>browserHistory.push('detail/'+article.articleId)}>
                                        <p>{article.context}</p>
                                    </div>
                                </div>
                            </div>
                        )
                    })
                }
            {/*{文章列表1}*/}
          <div className="ui divided items">
              {
                  Empty.check(this.state.data)?this.state.data.map((article, index, articleList) => {
                      return (
                          <div className="item" key={index}>
                            <div className="ui tiny image" style={{textAlign:'center'}}>
                              <img src={Empty.check(article.head)?article.head:Head}/>
                              <span className="price">{article.nickName}</span>
                            </div>
                            <div className="content">
                              <div className="header">{article.title}</div>
                                <Cell changeType={this.changeType} profile={myProfile} index={index} onDelete={this.onDelete} articleId={article.userId}/>
                              <div className="description" style={{cursor:'pointer'}} onClick={()=>browserHistory.push('detail/'+index)}>
                                <p>{article.context}</p>
                              </div>
                                {Empty.check(article.image)?<img src={article.image} style={{width:500,height:300}}/>:false}
                                </div>
                          </div>
                      )
                  }):<p>数据加载中...</p>
              }
          </div>
        </div>
        </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    myProfile: state.user.myProfile,
    articleList: state.article.articleList,
    articleListStatus: state.common.asyncStatus.articleList || {},
      createArticles:state.article.createArticles
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({...UserActions, ...ArticleActions, ...CommonActions}, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)