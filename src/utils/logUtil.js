/*import Log4js from 'log4js'

 Log4js.configure({
 appenders: [
 { type: 'console' }
 ]
 })

 export function getLogger(name){
 let logger = Log4js.getLogger(name)
 logger.setLevel('TRACE')
 return logger
 }*/


export function lc(classObj, funcName){
  console.log('[LC][' + classObj.constructor.name + ']' + '[' + funcName + ']')
}

export function log(classObj, content){
  console.log('[LOG][' + classObj.constructor.name + '] ---- \r\n %O', content)
}
