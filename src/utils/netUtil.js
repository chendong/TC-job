import Empty from './emptyUtil'

export function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response.json()
  } else {
    var error = new Error(response.statusText)
    error.response = response
    throw error
  }
}

export function parseJSON(response) {
  return response.json()
}


/**
 * 自定义Fetch
 */
export function myFetch(url, options) {

  return new Promise(function (resolve, reject) {
   // console.log(options.header)
    var xhr = new XMLHttpRequest()

    xhr.open(options.method, url, true)

    xhr.onload = function () {
      if (xhr.status == 200) {
        console.log('request:\n' + 'url:' + url + '\n' + 'method: ' + options.method + '\n' + 'body: ' + options.body)
        //console.log('response: \n' + xhr.response)
        resolve(JSON.parse(xhr.response))
      }
      else {
        console.log('request:\n' + 'url:' + url + '\n' + 'method: ' + options.method + '\n' + 'body: ' + options.body)
        console.log('response: \n' + JSON.stringify(xhr.response))
        reject(Error(xhr.statusText))
      }
    }

    xhr.onerror = function () {
      console.log('request:\n' + 'url:' + url + '\n' + 'method: ' + options.method + '\n' + 'body: ' + options.body)
      console.log('response: \n' + 'Network Error')
      reject(Error("Network Error"))
    }
    if (options.method === 'POST') {
      xhr.send(options.body)
    } else {
      xhr.send()
    }

  })
}

