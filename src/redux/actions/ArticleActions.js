/**
 * Created by Melon on 2017/4/6.
 */
import {createAction, handleAction, handleActions} from 'redux-actions'

import * as CommonActions from './CommonActions'
import * as ActionTypes from './ActionTypes'
import * as Api from '../../constants/Api'
import Empty from '../../utils/emptyUtil'
import {myFetch} from '../../utils/netUtil'

//获取文章列表
export const articleListReset = createAction(ActionTypes.ARTICLE_LIST_RESET)
export const articleListSuccess = createAction(ActionTypes.ARTICLE_LIST_SUCCESS)
export function acArticleList(params) {
  return (dispatch, getState) => {
    dispatch(CommonActions.asyncStatus({key: 'articleList', type: 'loading', msg: '获取文章列表中'}))
    myFetch(Api.APT_ARTICLE_LIST, {
      method: 'GET',
    })
        .then(json => {
          if (json.code == 200) {
            dispatch(articleListSuccess({articleList:json.entity}))
            dispatch(CommonActions.asyncStatus({key: 'articleList', type: 'success', msg: '获取文章列表成功'}))
          } else {
            dispatch(CommonActions.asyncStatus({key: 'articleList', type: 'error', msg: '获取文章列表失败'}))
          }
        })
        .catch(err => {
          dispatch(CommonActions.asyncStatus({key: 'articleList', type: 'error', msg: err.message}))
        })
  }
}

//获取文章详情
export const articleDetailReset = createAction(ActionTypes.ARTICLE_DETAIL_RESET)
export const articleDetailSuccess = createAction(ActionTypes.ARTICLE_DETAIL_SUCCESS)
export function acArticleDetail(params) {
  return (dispatch, getState) => {
    dispatch(CommonActions.asyncStatus({key: 'articleDetail', type: 'loading', msg: '获取文章详情中'}))
    let articleList=getState().article.articleList.slice();
    let articleId=params.articleId
    setTimeout(()=>{
      articleList.map((article,index,articleList)=>{
        if(article.articleId===articleId){
          dispatch(articleDetailSuccess({articleDetail:article}))
          dispatch(CommonActions.asyncStatus({key: 'articleDetail', type: 'success', msg: '获取文章详情成功'}))
        }
      },500)
    })
  }
}

//评论文章详情
export const commentArticleReset = createAction(ActionTypes.COMMENT_ARTICLE_RESET)
export const commentArticleSuccess = createAction(ActionTypes.COMMENT_ARTICLE_SUCCESS)
export function acCommentArticle(params) {
  return (dispatch, getState) => {
    dispatch(CommonActions.asyncStatus({key: 'commentArticle', type: 'loading', msg: '评论文章中'}))
    let articleDetail=Object.assign({},getState().article.articleDetail);
    let comments=articleDetail.comments||[]
    comments.unshift(params)
    articleDetail.comments=comments
    setTimeout(()=>{
          dispatch(commentArticleSuccess({articleDetail:articleDetail}))
          dispatch(CommonActions.asyncStatus({key: 'commentArticle', type: 'success', msg: '评论文章成功'}))
      },500)
  }
}



//发表文章
export const createArticleReset = createAction(ActionTypes.CREATE_ARTICLE_RESET)
export const createArticleSuccess = createAction(ActionTypes.CREATE_ARTICLE_SUCCESS)
export const fcau = createAction(ActionTypes.F_C_A_U)
export function acCreateArticle(params) {
  var fd = new FormData()
  fd.append('title', params.title)
  fd.append('context', params.context)
  fd.append('userId',params.userId)
  return (dispatch, getState) => {
    dispatch(CommonActions.asyncStatus({key: 'createArticleList', type: 'loading', msg: '发表文章中'}))
    myFetch(Api.APT_CREATE_ARTICLE, {
      method: 'POST',
      body: fd,
    })
        .then(json => {
          if (json.code == 200) {
            dispatch(createArticleSuccess({createArticle:json.entity}))
            dispatch(CommonActions.asyncStatus({key: 'createArticleList', type: 'success', msg: '发表文章成功'}))
          } else {
            dispatch(CommonActions.asyncStatus({key: 'createArticleList', type: 'error', msg: '发表文章失败'}))
          }
        })
        .catch(err => {
          dispatch(CommonActions.asyncStatus({key: 'createArticleList', type: 'error', msg: err.message}))
        })
  }
}
export function acFCAU(params) {
    return(dispatch, getState)=>{
        dispatch(fcau(params))
    }
}