/**
 * Created by Melon on 2017/4/6.
 */
import {createAction, handleAction, handleActions} from 'redux-actions'

import * as CommonActions from './CommonActions'
import * as ActionTypes from './ActionTypes'
import * as Api from '../../constants/Api'
import Empty from '../../utils/emptyUtil'
import {myFetch} from '../../utils/netUtil'


//注册
export function asyncRegister(params) {
  var fd = new FormData()
  fd.append('account', params.account)
  fd.append('passWord', params.passWord)
  fd.append('nickName', params.nickName)
    fd.append('head', params.head)
    fd.append('sex', params.sex)
  return (dispatch, getState) => {
    dispatch(CommonActions.asyncStatus({key: 'register', type: 'loading', msg: '注册中'}))
    myFetch(Api.API_REGISTER, {
      method: 'POST',
      body: fd,
    })
        .then(json => {
          if (json.code == 200) {
            dispatch(userLoginSuccess({myProfile:json.entity}))
            //将userId存储至cookie
            let userId=json.entity.userId
            let expires=new Date('2018-1-1')
            document.cookie="userId=${userId};expires=${expires.toGMTString()}"

            dispatch(CommonActions.asyncStatus({key: 'register', type: 'success', msg: '注册成功'}))
          } else {
            dispatch(CommonActions.asyncStatus({key: 'register', type: 'error', msg: '注册失败'}))
          }
        })
        .catch(err => {
          dispatch(userLoginReset())
          dispatch(CommonActions.asyncStatus({key: 'register', type: 'error', msg: err.message}))
        })
  }
}

//登录
export const userLoginReset = createAction(ActionTypes.USER_LOGIN_RESET)
export const userLoginSuccess = createAction(ActionTypes.USER_LOGIN_SUCCESS)
export function acLogin(params) {

  return (dispatch, getState) => {
    dispatch(CommonActions.asyncStatus({key: 'login', type: 'loading', msg: '登录中'}))
    myFetch(Api.API_LOGIN+'?account='+params.account+'&password='+params.password, {
      method: 'POST',
    })
      .then(json => {
        if (json.code == 200) {
          dispatch(userLoginSuccess({myProfile:json.entity}))
          //将userId存储至cookie
          let userId=json.entity.id
          let expires=new Date('2018-1-1')
          document.cookie=`userId=${userId};expires=${expires.toGMTString()}`
          dispatch(CommonActions.asyncStatus({key: 'login', type: 'success', msg: '登录成功'}))
        } else {
          dispatch(CommonActions.asyncStatus({key: 'login', type: 'error', msg: '登录失败'}))
        }
      })
      .catch(err => {
        dispatch(CommonActions.asyncStatus({key: 'login', type: 'error', msg: err.message}))
      })
  }
}

//退出登录
export function userLogout(){
  return (dispatch, getState) => {
    //清楚cookie中的userId
    let expires=new Date('1970-1-1')
    document.cookie=`userId= ;expires=${expires.toGMTString()}`

    dispatch(userLoginReset())
  }
}

//根据userId获取用户个人信息
export const myProfileReset = createAction(ActionTypes.MY_PROFILE_RESET)
export const myProfileSuccess = createAction(ActionTypes.MY_PROFILE_SUCCESS)
export function acMyProfile(params){
  return (dispatch, getState) => {
    dispatch(CommonActions.asyncStatus({key: 'myProfile', type: 'loading', msg: '获取个人信息中'}))
    myFetch(Api.API_MY_PROFILE+'?userId='+params, {
      method: 'GET',
    })
        .then(json => {
          if (json.code == 200) {
            dispatch(myProfileSuccess({myProfile:json.entity}))
            dispatch(CommonActions.asyncStatus({key: 'myProfile', type: 'success', msg: '获取个人信息成功'}))
          } else {
            dispatch(CommonActions.asyncStatus({key: 'myProfile', type: 'error', msg: '获取个人信息失败'}))
          }
        })
        .catch(err => {
          dispatch(myProfileReset())
          dispatch(CommonActions.asyncStatus({key: 'myProfile', type: 'error', msg: err.message}))
        })
  }
}