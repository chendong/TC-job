/**
 * Created by Melon on 2017/4/6.
 */
import {createAction, handleAction, handleActions} from 'redux-actions'

import * as ActionTypes from '../../redux/actions/ActionTypes'
import * as Api from '../../constants/Api'
import Empty from '../../utils/emptyUtil'
import {myFetch} from '../../utils/netUtil'


export const asyncStatusReset = createAction(ActionTypes.ASYNC_STATUS_RESET);
export const asyncStatusLoading = createAction(ActionTypes.ASYNC_STATUS_LOADING);
export const asyncStatusSuccess = createAction(ActionTypes.ASYNC_STATUS_SUCCESS);
export const asyncStatusError = createAction(ActionTypes.ASYNC_STATUS_ERROR);

export function asyncStatus(params) {
  let {key, type} = params;
  let msg = params.msg || '';
  return (dispatch, getState) => {
    if (type === 'loading') {
      let asyncStatus = Object.assign({}, getState().common.asyncStatus);
      asyncStatus[key] = {key: key, type: 'loading', msg: msg};
      dispatch(asyncStatusLoading({asyncStatus: asyncStatus}))
    }
    if (type === 'success') {
      let asyncStatus = Object.assign({}, getState().common.asyncStatus);
      asyncStatus[key] = {key: key, type: 'success', msg: msg};
      dispatch(asyncStatusSuccess({asyncStatus: asyncStatus}))
    }
    if (type === 'error') {
      let asyncStatus = Object.assign({}, getState().common.asyncStatus);
      asyncStatus[key] = {key: key, type: 'error', msg: msg};
      dispatch(asyncStatusError({asyncStatus: asyncStatus}))
    }
    if (type === 'reset') {
      let asyncStatus = Object.assign({}, getState().common.asyncStatus);
      asyncStatus[key] = {key: key, type: 'reset', msg: msg};
      dispatch(asyncStatusReset({asyncStatus: asyncStatus}))
    }
  }
}

//userToken
export const tokenReset = createAction(ActionTypes.TOKEN_RESET);
export const tokenSuccess = createAction(ActionTypes.TOKEN_SUCCESS);

export function localStorageToken(params) {
  let {type} = params;
  return (dispatch, getState) => {
    if (type === 'success') {
      let token = params.token || '';
      //本地存储token
      // storage.save({
      //   key: 'token',
      //   rawData: token
      // });
      dispatch(tokenSuccess({token: token}));
    }
    if (type === 'reset') {
      //删除本地token
      // storage.remove({
      //   key: 'token'
      // });
      dispatch(tokenReset());
    }
  }
}

//上传图片
export const picUploadReset = createAction(ActionTypes.PIC_UPLOAD_RESET)
export const picUploadSuccess = createAction(ActionTypes.PIC_UPLOAD_SUCCESS)

export function acPicUpload(params) {
  var fd = new FormData()
  fd.append('pictureFile', params)
  return (dispatch, getState) => {
    dispatch(asyncStatus({key: 'picUpload', type: 'loading', msg: '图片正在上传'}))
    myFetch(Api.API_PIC_UPLOAD, {
      method: 'POST',
      body: fd,
    })
      .then(json => {
        if (json.code == 200) {
          dispatch(picUploadSuccess(json.entity))
        } else {
          dispatch(asyncStatus({key: 'picUpload', type: 'error', msg: '图片上传失败'}))
        }
      })
        .catch(err=>{
          dispatch(asyncStatus({key: 'picUpload', type: 'error', msg: err.message}))
        })
  }
}
export function acPicUploadReset() {
    return(dispatch, getState)=>{
      dispatch(picUploadReset())
    }
}
