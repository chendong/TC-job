/**
 * Created by Melon on 2016/11/16.
 */
import {handleAction, handleActions} from 'redux-actions'

let initialState = {
  articleList: [],
  createArticle:{},
  createArticles:[{
      title: 'React入门',
      context: '现在最热门的前端框架，毫无疑问是 React上周，基于 React 的 React Native 发布，结果一天之内，就获得了 5000 颗星，受瞩目程度可见一斑。React 起源于 Facebook 的内部项目，因为该公司对市场上所有 JavaScript MVC 框架，都不满意，就决定自己写一套，用来架设 Instagram 的网站。做出来以后，发现这套东西很好用，就在2013年5月开源了。由于 React 的设计思想极其独特，属于革命性创新，性能出众，代码逻辑却非常简单。所以，越来越多的人开始关注和使用，认为它可能是将来 Web 开发的主流工具。这个项目本身也越滚越大，从最早的UI引擎变成了一整套前后端通吃的 Web App 解决方案。衍生的 React Native 项目，目标更是宏伟，希望用写 Web App 的方式去写 NativeApp。如果能够实现，整个互联网行业都会被颠覆，因为同一组人只需要写一次 UI ，就能同时运行在服务器、浏览器和手机（参见《也许，DOM 不是答案》）。',
      image: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1494314022404&di=6e848b3a0490d8ad2075da17795fbb6f&imgtype=0&src=http%3A%2F%2Fimg.sdk.cn%2Ftools%2F201504%2F2274955b1e770248.png',
      head:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1494311477094&di=1a0e0a664c97f4733fc866d38ba37b13&imgtype=0&src=http%3A%2F%2Fwww.chuanke.com%2Fupload%2Fcourseware%2Ff%2F31%2F3312428%2Fimage%2F09c68fe797fa58d78a1de4f34e0ea40f.gif',
      nickName:'Mr.React'
  },{
      title: 'React 技术栈',
      context: '上周中秋节，我待在家里，写完了 Redux 教程。至此，《React 技术栈系列教程》算是比较完整，这两年没停过，一直在学习新东西，学习心得就写成了上面的教程。虽然看上去数量不少，但是下一代的互联网开发技术，我还是只学了很小一部分，像 PostCSS、GraphQL、Electron 这些感兴趣的东西，都没时间搞。面对技术的高速发展和百花齐放，我有时也感到疲倦烦躁。但是，每当看到它们带来的生产力的飞跃，让你一个人快速搞定前后端的全部开发时，就觉得这终究还是一条正确的道路。',
      image: 'http://www.ruanyifeng.com/blogimg/asset/2016/bg2016092303.jpg',
      head:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1494314255655&di=7067d7d0d3039697fb86326278bef455&imgtype=0&src=http%3A%2F%2Fimg5.duitang.com%2Fuploads%2Fitem%2F201603%2F04%2F20160304174443_42UTn.thumb.224_0.jpeg',
      nickName:'Mrs.React'
  },{
      title: 'React Native从入门到原理',
      context: 'React Native 是最近非常火的一个话题，介绍如何利用 React Native 进行开发的文章和书籍多如牛毛，但面向入门水平并介绍它工作原理的文章却寥寥无几。本文分为两个部分：上半部分用通俗的语言解释了相关的名词，重点介绍 React Native 出现的背景和试图解决的问题。适合新手对 React Native 形成初步了解。(事实证明，女票能看懂这段）下半部分则通过源码（0.27 版本）分析 React Native 的工作原理，适合深入学习理解 React Native 的运行机制。最后则是我个人对 React Native 的分析与前景判断。动态配置由于 AppStore 审核周期的限制，如何动态的更改 app 成为了永恒的话题。无论采用何种方式，我们的流程总是可以归结为以下三部曲：“从 Server 获取配置 --> 解析 --> 执行native代码”。很多时候，我们自觉或者不自觉的利用 JSON 文件实现动态配置的效果，它的核心流程是：通过 HTTP 请求获取 JSON 格式的配置文件。配置文件中标记了每一个元素的属性，比如位置，颜色，图片 URL 等。解析完 JSON 后，我们调用 Objective-C 的代码，完成 UI 控件的渲染。通过这种方法，我们实现了在后台配置 app 的展示样式。从本质上来说，移动端和服务端约定了一套协议，但是协议内容严重依赖于应用内要展示的内容，不利于拓展。也就是说，如果业务要求频繁的增加或修改页面，这套协议很难应付。最重要的是，JSON 只是一种数据交换的格式，说白了，我们就是在解析文本数据。这就意味着它只适合提供一些配置信息，而不方便提供逻辑信息。举个例子，我们从后台可以配置颜色，位置等信息，但如果想要控制 app 内的业务逻辑，就非常复杂了。记住，我们只是在解析字符串，它完全不具备运行和调试的能力。',
      image: 'http://upload-images.jianshu.io/upload_images/1171077-75412d65af198cf5.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240',
      head:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1494314301396&di=7360d0e97207c8f479e97e20b3e0131c&imgtype=0&src=http%3A%2F%2Fimgtu.5011.net%2Fuploads%2Fcontent%2F20170310%2F9471881489131959.jpg',
      nickName:'Mr.RNative'
  },{
      title: 'Spring MVC',
      context: 'Spring MVC属于SpringFrameWork的后续产品，已经融合在Spring Web Flow里面。Spring 框架提供了构建 Web 应用程序的全功能 MVC 模块。使用 Spring 可插入的 MVC 架构，从而在使用Spring进行WEB开发时，可以选择使用Spring的SpringMVC框架或集成其他MVC开发框架，如Struts1，Struts2等',
      image: '',
      head:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1494311477094&di=1a0e0a664c97f4733fc866d38ba37b13&imgtype=0&src=http%3A%2F%2Fwww.chuanke.com%2Fupload%2Fcourseware%2Ff%2F31%2F3312428%2Fimage%2F09c68fe797fa58d78a1de4f34e0ea40f.gif',
      nickName:'Mr.React'
  },{
      title: 'Webpack',
      context: 'webpack 是一个现代的 JavaScript 应用程序的模块打包器(module bundler)。当 webpack 处理应用程序时，它会递归地构建一个依赖关系图表(dependency graph)，其中包含应用程序需要的每个模块，然后将所有这些模块打包成少量的 bundle - 通常只有一个，由浏览器加载。',
      image: 'https://doc.webpack-china.org/bf093af83ee5548ff10fef24927b7cd2.svg',
      head:'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1494311477094&di=1a0e0a664c97f4733fc866d38ba37b13&imgtype=0&src=http%3A%2F%2Fwww.chuanke.com%2Fupload%2Fcourseware%2Ff%2F31%2F3312428%2Fimage%2F09c68fe797fa58d78a1de4f34e0ea40f.gif',
      nickName:'Mr.React'
  },],
  articleDetail:{}

}

export default handleActions({

  ARTICLE_LIST_SUCCESS: (state, action) => ({
    ...state,
    articleList: action.payload.articleList
  }),
  ARTICLE_LIST_RESET: (state, action) => ({
    ...state,
    articleList: []
  }),

  ARTICLE_DETAIL_SUCCESS: (state, action) => ({
    ...state,
    articleDetail: action.payload.articleDetail
  }),
  ARTICLE_DETAIL_RESET: (state, action) => ({
    ...state,
    articleDetail: {}
  }),

  COMMENT_ARTICLE_SUCCESS: (state, action) => ({
    ...state,
    articleDetail: action.payload.articleDetail
  }),
  COMMENT_ARTICLE_RESET: (state, action) => ({
    ...state,
    articleDetail: {}
  }),

  CREATE_ARTICLE_SUCCESS: (state, action) => ({
    ...state,
    createArticle: action.payload.createArticle
  }),
  CREATE_ARTICLE_RESET: (state, action) => ({
    ...state,
    createArticleList: {}
  }),
    FA_CREATE_ARTICLE_UPDATE: (state, action) => ({
        ...state,
        createArticles: action.payload
    }),


}, initialState)