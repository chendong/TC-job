/**
 * Created by Melon on 2017/4/6.
 */
import {handleAction, handleActions} from 'redux-actions'

let initialState = {
  asyncStatus: {},
  picUploadUrl: ''
}

export default handleActions({

  ASYNC_STATUS_RESET: (state, action) => ({
    ...state,
    asyncStatus: action.payload.asyncStatus
  }),

  ASYNC_STATUS_LOADING: (state, action) => ({
    ...state,
    asyncStatus: action.payload.asyncStatus
  }),

  ASYNC_STATUS_SUCCESS: (state, action) => ({
    ...state,
    asyncStatus: action.payload.asyncStatus
  }),

  ASYNC_STATUS_ERROR: (state, action) => ({
    ...state,
    asyncStatus: action.payload.asyncStatus
  }),

  PIC_UPLOAD_SUCCESS: (state, action) => ({
    ...state,
    picUploadUrl: action.payload
  }),
  PIC_UPLOAD_RESET: (state, action) => ({
    ...state,
    picUploadUrl: ''
  }),

}, initialState)