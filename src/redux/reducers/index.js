import { combineReducers } from 'redux'
import { syncHistoryWithStore, routerReducer } from 'react-router-redux'

import UserReducer from './UserReducer'
import ArticleReducer from './ArticleReducer'
import CommonReducer from './CommonReducer'

const rootReducer = combineReducers({
  user:UserReducer,
  article:ArticleReducer,
  common:CommonReducer,
  routing: routerReducer,
})

export default rootReducer
