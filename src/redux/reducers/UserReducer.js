/**
 * Created by Melon on 2016/11/16.
 */
import {handleAction, handleActions} from 'redux-actions'

let initialState = {
  myProfile: {}
}

export default handleActions({

  USER_LOGIN_SUCCESS: (state, action) => ({
    ...state,
    myProfile: action.payload.myProfile
  }),
  USER_LOGIN_RESET: (state, action) => ({
    ...state,
    myProfile: {}
  }),

  MY_PROFILE_SUCCESS: (state, action) => ({
    ...state,
    myProfile: action.payload.myProfile
  }),
  MY_PROFILE_RESET: (state, action) => ({
    ...state,
    myProfile: {}
  })

}, initialState)