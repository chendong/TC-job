import {createStore, applyMiddleware, combineReducers, compose} from 'redux'
import thunkMiddleware from 'redux-thunk'
import {createLogger} from 'redux-logger'

import rootReducer from '../reducers'

export default function configureStore(initialState) {
  let store

  console.log('--------------NODE_ENV: '+process.env.NODE_ENV+'--------------')

  if (process.env.NODE_ENV==='production') {
    store = createStore(
      rootReducer,
      initialState,
      compose(
        applyMiddleware(thunkMiddleware),
      )
    )
  } else {
    store = createStore(
      rootReducer,
      initialState,
      compose(
        applyMiddleware(thunkMiddleware, createLogger())
      )
    )
  }
  return store
}
