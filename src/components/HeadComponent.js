/**
 * Created by Melon on 2016/10/22.
 */
import React, {Component, PropTypes} from 'react'
import {Link, browserHistory} from 'react-router'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

import * as UserActions from '../redux/actions/UserActions'
import * as CommonActions from '../redux/actions/CommonActions'
import Empty from '../utils/emptyUtil'
import Img1 from "../assert/image/user-head.png"
var Head = 'http://steelgang-pro.oss-cn-beijing.aliyuncs.com/avatar/avatar_default_1.jpg'
class HeadComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {}
    this.logout = this.logout.bind(this)
  }

  componentWillMount() {

  }


  componentDidMount() {

  }

  componentDidUpdate(prevProps, prevState) {

  }

  //退出登录
  logout() {
    this.props.actions.userLogout()
  }

  render() {
    let {myProfile, route} = this.props;
    return (
        <div className="ui secondary  menu">
          <Link className={"item " + (route.pathname === "/" ? "active" : "")} to="/">优友</Link>
          <Link className={"item " + (route.pathname === "create" ? "active" : "")} to="create">发表</Link>
          <div className="right menu">
            <div className="item">
              <div className="ui icon input">
                <input type="text" placeholder="Search..."/>
                <i className="search link icon"></i>
              </div>
            </div>
            {
              Empty.check(myProfile) ?
                  (() => {
                    return (
                        <div className="item">
                          <img className="ui avatar image" src={Empty.check(myProfile.head)?myProfile.head:Head}/>
                          <span>{myProfile.nickName}</span>
                          <a className="ui item" onClick={this.logout}>退出登录</a>
                        </div>)
                  })() : <Link className="ui item" to="login">登录</Link>
            }
          </div>
        </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    myProfile: state.user.myProfile,
    route: state.routing.locationBeforeTransitions
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({...UserActions, ...CommonActions}, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HeadComponent)