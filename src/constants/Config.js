import path from 'path'

let rootPath = path.normalize(__dirname + '/..')
let env = process.env.API_ENV || ENV_CHOOSEN

const Config = {
  local: {
    API_HOST: "http://localhost:3001",
    ALI_OSS_URL: "steelgang-dev.oss-cn-beijing.aliyuncs.com"
  },

};

export default Config[env]
