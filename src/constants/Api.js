/**
 * Created by Melon on 2016/11/16.
 */

const API_HOST='http://112.74.30.134:8080'
//登录
export const API_LOGIN = API_HOST+'/Oldriver/user/login'
//注册
export const API_REGISTER = API_HOST+'/Oldriver/user/register'
//根据userId获取用户个人信息
export const API_MY_PROFILE = API_HOST+'/Oldriver/user/queryUserInfo'
//图片上传地址
export const API_PIC_UPLOAD = API_HOST+'/Oldriver/file/saveFile'

//文章列表
export const APT_ARTICLE_LIST = API_HOST+'/Oldriver/article/queryAllArticle'
//发表文章
export const APT_CREATE_ARTICLE = API_HOST+'/Oldriver/article/saveArticle'