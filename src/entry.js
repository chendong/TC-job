/**
 * Created by Melon on 2016/10/21.
 */
import React,{Component,PropTypes} from 'react'
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import { Router, Route, Redirect, IndexRoute,browserHistory } from 'react-router'
import { syncHistoryWithStore, routerReducer } from 'react-router-redux'

import configureStore from './redux/store/configureStore'

import '../semantic/dist/semantic.min.css'
import '../semantic/dist/semantic.min.js'

import Home from './views/Home';
import Create from './views/Create'
import Login from './views/Login'
import Detail from './views/Detail'


const store = configureStore()
const history = syncHistoryWithStore(browserHistory, store)


ReactDOM.render(
    <Provider store={store}>
        <Router history={history}>
          <Route exact path="/" component={Home} />
          <Route exact path="login" component={Login} />
          <Route exact path="create" component={Create} />
          <Route exact path="detail/:id" component={Detail} />
        </Router>
    </Provider>,
    document.getElementById('main')
)
