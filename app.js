/**
 * Created by luojian on 17-5-5.
 */
let NODE_ENV=process.env.NODE_ENV
let app

if(NODE_ENV==='production'){
  app=require('./server/server.dev')
}else{
  app=require('./server/server.prod')
}

app.listen(3000, function (err, result) {
  if (err) {
    console.log(err)
  }
  console.log('NODE_ENV:'+process.env.NODE_ENV+',Listening at localhost:3000')
})
