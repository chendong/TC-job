/**
 * Created by Melon on 2016/10/21.
 */
var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  target: 'web',
  entry: path.resolve(__dirname, 'src/entry.js'),  // app的入口文件
  output: {
    filename: 'js/[name].bundle.js', // 输出的打包文件
    path: path.resolve(__dirname, 'dist'),  //输出打包文件的路径
    publicPath: '/'   // 对于热替换（HMR）是必须的，让webpack知道在哪里载入热更新的模块（chunk）
  },
  devtool: 'inline-source-map',  //开发环境下的source-map配置,SourceMap 转换为 DataUrl 后添加到 bundle 中
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env']
          }
        }
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: ['css-loader', 'sass-loader'] //resolve-url-loader may be chained before sass-loader if necessary
        })
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({   //使用ExtractTextWebpackPlugin为所有CSS模块生成行的bundle,以利用浏览器的异步和并行加载CSS的能力
          fallback: 'style-loader',
          use: 'css-loader'
        })
      },
      {
        test: /\.(jpg|png|gif)$/,
        use: 'file-loader'
      },
      {
        test: /\.(woff|woff2|eot|ttf|svg)$/,
        use: 'url-loader?limit=100000'
      },
      {
        test: /\.json$/,
        use: 'json-loader'
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: path.resolve(__dirname, 'index.html'),
      favicon: path.resolve(__dirname, 'favicon.ico'),
      hash: false
    }),

    new webpack.HotModuleReplacementPlugin(), // 开启全局的模块热替换（HMR）
    new webpack.NamedModulesPlugin(), // 当模块热替换（HMR）时在浏览器控制台输出对用户更友好的模块名字信息
    new webpack.NoErrorsPlugin(),

    //分离第三方库(vendor)，以利用浏览器缓存优化
    // new webpack.optimize.CommonsChunkPlugin({
    //     names: ['vendor', 'manifest'] // 指定公共 bundle 的名字。
    // }),

    // extract css into its own file
    new ExtractTextPlugin({
      filename: 'css/[name].bundle.css'
    }),

    //DefinePlugin,定义全局变量的常量,在原始的源码中执行查找和替换操作. 在导入的代码中,任何出现 process.env.NODE_ENV的地方都会被替换为"development".
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('development')
      },
      jquery: "$",
      jQuery: "$",
    })
  ],
  resolve: {
    extensions: [' ', '.js', '.json'],
    modules: ['node_modules']
  }
  ,
//jQuery从CND获取，不打包jQuery
  externals: {
    jquery: 'jQuery'
  }
}
;
